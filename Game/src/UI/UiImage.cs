using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {

	/// <summary>
	/// UiWidget for displaying a texture
	/// </summary>
	public class UiImage : UiWidget {
		/// Texture2D to display
		public Texture2D texture = null;

		/// constructor
		public UiImage(Ui ui, UiWidget parent = null) : base(ui, parent) {
			this.ui.AddDrawable(this);
		}

		/// Draw the UiImage
		public override void Draw(SpriteBatch spriteBatch) {
			if(texture != null) {
				Rectangle rect = new Rectangle(bounds.Location, bounds.Size);
				UiWidget parent = GetParent();
				while(parent != null) {
					rect.X += parent.bounds.X;
					rect.Y += parent.bounds.Y;
					parent = parent.GetParent();
				}
				spriteBatch.Draw(texture, rect, Color.White);
			}
			base.Draw(spriteBatch);
		}

		/// Can't select an image
		public override bool CanSelect() {
			return base.CanSelect() && false;
		}
	}

}