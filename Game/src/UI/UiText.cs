using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {

	/// <summary>
	/// UiWidgets for displaying text.
	/// </summary>
	public class UiText : UiWidget {
		/// The font that displays the text
		public SpriteFont font = null;
		/// The text
		public string text = "";
		/// Text color
		public Color textColor = Color.White;
		/// Text shadow color
		public Color textShadowColor = Color.Black;
		/// Whether to draw text shadow
		public bool drawShadow = true;

		/// constructor
		public UiText(Ui ui, UiWidget parent = null) : base(ui, parent) {
			this.ui.AddDrawable(this);
		}

		/// Draw the text to the sprite batch
		public override void Draw(SpriteBatch spriteBatch) {
			if(!string.IsNullOrEmpty(text) && font != null) {
				Vector2 position = bounds.Location.ToVector2();
				UiWidget parent = GetParent();
				while(parent != null) {
					position += parent.bounds.Location.ToVector2();
					parent = parent.GetParent();
				}

				if(drawShadow) {
					spriteBatch.DrawString(font, text, position + new Vector2(1.0f, 1.0f), textShadowColor);
				}
				spriteBatch.DrawString(font, text, position, textColor);
			}

			base.Draw(spriteBatch);
		}

		/// Can't select a UiText
		public override bool CanSelect() {
			return base.CanSelect() && false;
		}
	}

}