using Microsoft.Xna.Framework.Content;

namespace RpgGame {

	/// <summary>
	/// Root ui object
	/// </summary>
	public class UiRoot : UiWidget {
		/// content manager for all widgets that is child to this root
		protected ContentManager contentManager = null;

		/// constructor
		public UiRoot(Ui ui) : base(ui, null) {
			contentManager = new ContentManager(App.instance.Services, App.instance.contentRoot);
		}

		/// Called from Ui when it has set the current root to this UiRoot
		public virtual void Initialize() {}
		/// Called from Ui when it has set the current root to another UiRoot
		public virtual void Release() {}

		/// Can't select a UiRoot widget
		public override bool CanSelect() { return base.CanSelect() && false; }

		/// Called when the back button have been pressed (you might want to switch UiRoot's here)
		public virtual void OnBack() {}
	}

}