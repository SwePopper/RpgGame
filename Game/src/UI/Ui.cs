using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace RpgGame {

	/// <summary>
	/// Main ui class
	/// </summary>
	public class Ui {
		/// Ui spriteBatch's GraphicsDevice
		public GraphicsDevice graphicsDevice { get { return spriteBatch?.GraphicsDevice; } }

		/// Image to ise when drawing the cursor
		public Texture2D cursor = null;

		/// Ui input handling class
		public UiInput input { get; private set; }

		/// checks if currently displaying a UiRoot widget
		public bool hasRoot { get { return currentRoot != null; } }

		SpriteBatch spriteBatch = null;

		UiRoot currentRoot = null;
		List<UiWidget> selectables = new List<UiWidget>();
		List<UiWidget> drawables = new List<UiWidget>();

		UiWidget currentSelected = null;
		UiWidget pressedWidget = null;

		/// constructor
		public Ui() {
		}

		/// Initializes the ui manager
		public void Initialize() {
			input = new UiInput();
			input.onPointerMovement += OnPointerMovementEvent;
			input.onPointerPressed += OnLeftPressed;
			input.onPointerReleased += OnLeftReleased;
			input.onAccept += OnAccept;
			input.onBack += OnBack;
			input.onMovedUp += OnMoveUp;
			input.onMovedLeft += OnMoveLeft;
			input.onMovedDown += OnMoveDown;
			input.onMovedRight += OnMoveRight;
		}

		/// Creates the spritebatch and calls LoadContent on the UiRoot root object
		public void LoadContent(GraphicsDevice graphicsDevice) {
			spriteBatch = new SpriteBatch(graphicsDevice);
			currentRoot?.LoadContent(graphicsDevice);
		}

		/// Updates the ui
		public void Update() {
			// Remove null objects
			selectables.RemoveAll(x => x == null);
			input.Update();
		}

		/// Draws the ui
		public void Draw() {
			// Remove null objects
			drawables.RemoveAll(x => x == null);
			// Sort back to front so alpha should be ok
			drawables.Sort((x, y) => { return x.drawDepth - y.drawDepth; });

			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
			for(int i = 0; i < drawables.Count; ++i) {
				// If not hidden and is inside viewport
				if(!drawables[i].hidden && drawables[i].bounds.Intersects(spriteBatch.GraphicsDevice.Viewport.Bounds)) {
					drawables[i].Draw(spriteBatch);
				}
			}

			if(cursor != null) {
				spriteBatch.Draw(cursor, Mouse.GetState().Position.ToVector2(), Color.White);
			}

			spriteBatch.End();
		}

		/// Creates UiRoot T and sets it as current
		public T SetCurrentRoot<T>() where T : UiRoot {
			// idk if this is AOT.. needs testing.
			T root = Activator.CreateInstance(typeof(T), new Object[] { this } ) as T;
			SetCurrentRoot(root);
			return root;
		}

		/// Sets the current root
		public void SetCurrentRoot(UiRoot root) {
			currentRoot?.Release();

			selectables.Clear();
			drawables.Clear();
			currentSelected = null;
			pressedWidget = null;
			currentRoot = root;

			currentRoot?.Initialize();
		}

		/// Add a UiWidget to the selectable list
		public void AddSelectable(UiWidget selectable) {
			if(selectable != null) {
				selectables.Add(selectable);
				if(currentSelected == null && selectable.CanSelect()) {
					currentSelected = selectable;
					currentSelected.OnSelect();
				}
			}
		}

		/// Add a UiWidget to the drawable list
		public void AddDrawable(UiWidget drawable) {
			if(drawable != null) {
				drawables.Add(drawable);
			}
		}

		/// Set widget as the current selected widget
		public void Select(UiWidget widget) {
			if(pressedWidget != null || widget == null || !widget.CanSelect() || currentSelected == widget)
				return;
			
			if(currentSelected != null) {
				currentSelected.OnDeselect();
			}
			currentSelected = widget;
			currentSelected.OnSelect();
		}

#region Input Events

		void OnPointerMovementEvent(Point newPosition, Point movement) {
			if(pressedWidget != null)
				return;
			
			for(int i = 0; i < selectables.Count; ++i) {
				if(selectables[i].CanSelect() && selectables[i].bounds.Contains(newPosition)) {
					if(currentSelected == selectables[i])
						return;
					
					if(currentSelected != null) {
						currentSelected.OnDeselect();
					}
					currentSelected = selectables[i];
					currentSelected.OnSelect();
					break;
				}
			}
		}

		void OnLeftPressed(Point position) {
			if(currentSelected == null)
				return;
			
			if(currentSelected.bounds.Contains(position)) {
				currentSelected.OnPressed();
				pressedWidget = currentSelected;
			}
		}

		void OnLeftReleased(Point position) {
			if(pressedWidget != null && pressedWidget.bounds.Contains(position)) {
				pressedWidget.OnAccept();
				// Might disapear
				if(pressedWidget != null) {
					pressedWidget.OnSelect();
					currentSelected = pressedWidget;
				}
			} else if(pressedWidget != null) {
				pressedWidget.OnSelect();
			}
			pressedWidget = null;
		}

		/// Call OnAccept on the current selected UiWidget
		public void OnAccept() {
			if(currentSelected != null) {
				currentSelected.OnAccept();
			}
		}

		/// Call OnBack on the current UiRoot
		public void OnBack() {
			if(currentRoot != null) {
				currentRoot.OnBack();
			}
		}

#region Selection Movement

		/// Move selection up
		public void OnMoveUp() {
			if(pressedWidget != null)
				return;
			
			if(currentSelected == null) {
				SelectAny();
			} else {
				UiWidget nextSelection = GetNextSelection((cur, next) => 
					Math.Sign(cur.bounds.Y - next.bounds.Y) * (next.bounds.Location - cur.bounds.Location).MagnitudeSquared()
				);
				if(nextSelection != null) {
					currentSelected.OnDeselect();
					currentSelected = nextSelection;
					currentSelected.OnSelect();
				}
			}
		}

		/// Move selection left
		public void OnMoveLeft() {
			if(pressedWidget != null)
				return;
			
			if(currentSelected == null) {
				SelectAny();
			} else {
				UiWidget nextSelection = GetNextSelection((cur, next) => 
					Math.Sign(cur.bounds.X - next.bounds.X) * (next.bounds.Location - cur.bounds.Location).MagnitudeSquared()
				);
				if(nextSelection != null) {
					currentSelected.OnDeselect();
					currentSelected = nextSelection;
					currentSelected.OnSelect();
				}
			}
		}

		/// Move selection down
		public void OnMoveDown() {
			if(pressedWidget != null)
				return;
			
			if(currentSelected == null) {
				SelectAny();
			} else {
				UiWidget nextSelection = GetNextSelection((cur, next) => 
					Math.Sign(next.bounds.Y - cur.bounds.Y) * (next.bounds.Location - cur.bounds.Location).MagnitudeSquared()
				);
				if(nextSelection != null) {
					currentSelected.OnDeselect();
					currentSelected = nextSelection;
					currentSelected.OnSelect();
				}
			}
		}

		/// Move selection right
		public void OnMoveRight() {
			if(pressedWidget != null)
				return;
			
			if(currentSelected == null) {
				SelectAny();
			} else {
				UiWidget nextSelection = GetNextSelection((cur, next) => 
					Math.Sign(next.bounds.X - cur.bounds.X) * (next.bounds.Location - cur.bounds.Location).MagnitudeSquared()
				);
				if(nextSelection != null) {
					currentSelected.OnDeselect();
					currentSelected = nextSelection;
					currentSelected.OnSelect();
				}
			}
		}

		UiWidget GetNextSelection(Comparison<UiWidget> comparison) {
			UiWidget nextSelection = null;
			int currentDistance = int.MaxValue;

			for(int i = 0; i < selectables.Count; ++i) {
				if(selectables[i].CanSelect() && selectables[i] != currentSelected) {
					int distance = comparison(currentSelected, selectables[i]);
					if(distance > 0 && distance < currentDistance) {
						currentDistance = distance;
						nextSelection = selectables[i];
					}
				}
			}

			return nextSelection;
		}
			
		void SelectAny() {
			if(currentSelected == null) {
				for(int i = 0; i < selectables.Count; ++i) {
					if(selectables[i].CanSelect()) {
						currentSelected = selectables[i];
						currentSelected.OnSelect();
						break;
					}
				}
			}
		}

#endregion

#endregion
	}

}