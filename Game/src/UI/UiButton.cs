using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {

	/// <summary>
	/// UiWidget with button logic
	/// </summary>
	public class UiButton : UiWidget {
		/// The states that the UiButton can be in
		public enum State {
			/// Normal unselected state
			kNormal,
			/// Selected state
			kHighlight,
			/// Pressed state
			kPressed,
			/// Disabled state
			kDisabled
		}

		/// The texture that is displayed when the button is in state kNormal
		public Texture2D normalTexture = null;
		/// The texture that is displayed when the button is in state kHighlight
		public Texture2D highlightTexture = null;
		/// The texture that is displayed when the button is in state kPressed
		public Texture2D pressedTexture = null;
		/// The texture that is displayed when the button is in state kDisabled
		public Texture2D disabledTexture = null;

		/// Delegate for on click event
		public delegate void OnClickEvent();
		/// Will be called when the button is clicked
		public event OnClickEvent onClickEvent = null;

		State currentState = State.kNormal;

		/// Get/Set the disabled state
		public bool disabled { 
			get { return currentState == State.kDisabled; } 
			set { currentState = value ? State.kDisabled : currentState == State.kDisabled ? State.kNormal : currentState; }
		}

		/// constructor
		public UiButton(Ui ui, UiWidget parent = null) : base(ui, parent) {
			this.ui.AddDrawable(this);
			this.ui.AddSelectable(this);
		}

		/// Draw the UiButton
		public override void Draw(SpriteBatch spriteBatch) {
			Texture2D texture = normalTexture;
			if(currentState == State.kHighlight && highlightTexture != null) {
				texture = highlightTexture;
			} else if(currentState == State.kPressed && pressedTexture != null) {
				texture = pressedTexture;
			} else if(currentState == State.kDisabled && disabledTexture != null) {
				texture = disabledTexture;
			}

			if(texture != null) {
				Rectangle rect = new Rectangle(bounds.Location, bounds.Size);
				UiWidget parent = GetParent();
				while(parent != null) {
					rect.X += parent.bounds.X;
					rect.Y += parent.bounds.Y;
					parent = parent.GetParent();
				}
				spriteBatch.Draw(texture, rect, Color.White);
			}
			base.Draw(spriteBatch);
		}

		/// Calls the onClickEvent
		public override void OnAccept() {
			onClickEvent?.Invoke();
		}

		/// UiButton can be selected when it is not in disabled state
		public override bool CanSelect() {
			return base.CanSelect() && currentState != State.kDisabled;
		}

		/// Switch to kHighlight state if not disabled
		public override void OnSelect() {
			if(currentState != State.kDisabled) {
				currentState = State.kHighlight;
			}
			base.OnSelect();
		}

		/// Switch to kNormal state if not disabled
		public override void OnDeselect() {
			if(currentState != State.kDisabled) {
				currentState = State.kNormal;
			}
			base.OnDeselect();
		}

		/// Switch to kPressed state
		public override void OnPressed() {
			currentState = State.kPressed;
			base.OnPressed();
		}
	}

}