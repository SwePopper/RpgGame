using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {

	/// <summary>
	/// Base ui object
	/// </summary>
	public class UiWidget {
		/// Ui main class that this widget belongs to
		protected Ui ui = null;

		/// bounds
		public Rectangle bounds = new Rectangle(0, 0, 1, 1);

		/// This UiWidget's parent UiWidget
		UiWidget parent = null;
		/// This UiWidget's children
		public List<UiWidget> children { get; private set; } = new List<UiWidget>();

		/// Hidden drawable widget
		public bool hidden { get; set; } = false;
		/// Depth to draw
		public int drawDepth { get; set; }

		/// constructor
		public UiWidget(Ui ui, UiWidget parent = null) {
			this.ui = ui;
			SetParent(parent);
		}

		/// Get parent UiWidget
		public UiWidget GetParent() { return parent; }
		/// Set the parent UiWidget
		public void SetParent(UiWidget parent) {
			this.parent?.children?.Remove(this);
			this.parent = parent;				
			this.parent?.children?.Add(this);
		}

		/// Sets the width and sets the hight automaticaly with aspect from the texture
		public void SetHightWithAspect(int width, Texture2D texture) {
			if(width <= 0)
				return;
			
			bounds.Width = width;
			bounds.Height = GameMath.FloorToInt(((float)texture.Height / (float)texture.Width) * width);
		}

		/// Sets the height and sets the width automaticaly with aspect from the texture
		public void SetWidthWithAspect(int height, Texture2D texture) {
			bounds.Width = GameMath.FloorToInt(height / ((float)texture.Height / (float)texture.Width));
			bounds.Height = height;
		}

		/// Draw the UiWidget
		public virtual void Draw(SpriteBatch spriteBatch) {}

		/// Called when ui pressed the accept button
		public virtual void OnAccept() {}
		/// Desides if the UiWidget is selectable
		public virtual bool CanSelect() { return true; }
		/// Called when ui selects this object
		public virtual void OnSelect() {}
		/// Called when ui deselects this object
		public virtual void OnDeselect() {}
		/// Called when ui presses this object
		public virtual void OnPressed() {}

		/// Called when the main app get's the LoadContent event. Calls LoadContent on all child UiWidget
		public virtual void LoadContent(GraphicsDevice graphicsDevice) {
			for(int i = 0; i < children.Count; ++i) {
				children[i]?.LoadContent(graphicsDevice);
			}
		}
	}

}