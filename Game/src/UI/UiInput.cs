using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RpgGame {

	/// <summary>
	/// Ui input handling class
	/// </summary>
	public class UiInput {
		/// Delegate for pointer movement
		public delegate void OnPointerMovementEvent(Point newPosition, Point movement);
		/// Pointer movement event
		public event OnPointerMovementEvent onPointerMovement = null;

		/// Delegate for pointer event
		public delegate void OnPointerEvent(Point position);
		/// Pointer pressed event
		public event OnPointerEvent onPointerPressed = null;
		/// Pointer released event
		public event OnPointerEvent onPointerReleased = null;

		/// Delegate for ordinary event
		public delegate void OnEvent();
		/// Accept event
		public event OnEvent onAccept = null;
		/// Back event
		public event OnEvent onBack = null;
		/// Move up event
		public event OnEvent onMovedUp = null;
		/// Move left event
		public event OnEvent onMovedLeft = null;
		/// Move down event
		public event OnEvent onMovedDown = null;
		/// Move right event
		public event OnEvent onMovedRight = null;

		/// Delegate for keyboard key press
		public delegate void OnKeyboardKeyPressedEvent(Keys key);
		/// Keyboard key pressed event
		public event OnKeyboardKeyPressedEvent onKeyboardKeyPressed = null;

		/// Accept keys (default: Keys.Enter, Keys.Space)
		public Keys[] acceptKeyboardKeys = new Keys[] { Keys.Enter, Keys.Space };
		/// Back keys (default: Keys.Escape, Keys.Back)
		public Keys[] backKeyboardKeys = new Keys[] { Keys.Escape, Keys.Back };
		/// Move up keys (default: Keys.Up, Keys.W)
		public Keys[] upKeyboardKeys = new Keys[] { Keys.Up, Keys.W };
		/// Move left keys (default: Keys.Left, Keys.A)
		public Keys[] leftKeyboardKeys = new Keys[] { Keys.Left, Keys.A };
		/// Move down keys (default: Keys.Down, Keys.S)
		public Keys[] downKeyboardKeys = new Keys[] { Keys.Down, Keys.S };
		/// Move right keys (default: Keys.Right, Keys.D)
		public Keys[] rightKeyboardKeys = new Keys[] { Keys.Right, Keys.D };

		MouseState lastMouseState;
		KeyboardState lastKeyboardState;

		/// constructor
		public UiInput() {
			lastMouseState = Mouse.GetState();
			lastKeyboardState = Keyboard.GetState();
		}

		/// Update all the events
		public void Update() {
			UpdateMouseState();
			UpdateKeyboard();
		}

#region Mouse Update

		void UpdateMouseState() {
			MouseState currentMouse = Mouse.GetState();
			if(!lastMouseState.Equals(currentMouse)) {
				Point movement = currentMouse.Position - lastMouseState.Position;
				if(!movement.IsZero()) {
					onPointerMovement?.Invoke(currentMouse.Position, movement);
				}

				// Check left mouse pressed and released state
				if(currentMouse.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released) {
					onPointerPressed?.Invoke(currentMouse.Position);
				} else if(currentMouse.LeftButton == ButtonState.Released && lastMouseState.LeftButton == ButtonState.Pressed) {
					onPointerReleased?.Invoke(currentMouse.Position);
				}

				lastMouseState = currentMouse;
			}
		}

#endregion

#region Keyboard Update

		void UpdateKeyboard() {
			KeyboardState currentKeyboard = Keyboard.GetState();
			if(!lastKeyboardState.Equals(currentKeyboard)) {
				if(IsKeysReleased(lastKeyboardState, currentKeyboard, acceptKeyboardKeys)) {
					onAccept?.Invoke();
				}

				if(IsKeysReleased(lastKeyboardState, currentKeyboard, backKeyboardKeys)) {
					onBack?.Invoke();
				}

				if(IsKeysReleased(lastKeyboardState, currentKeyboard, upKeyboardKeys)) {
					onMovedUp?.Invoke();
				}

				if(IsKeysReleased(lastKeyboardState, currentKeyboard, leftKeyboardKeys)) {
					onMovedLeft?.Invoke();
				}

				if(IsKeysReleased(lastKeyboardState, currentKeyboard, downKeyboardKeys)) {
					onMovedDown?.Invoke();
				}

				if(IsKeysReleased(lastKeyboardState, currentKeyboard, rightKeyboardKeys)) {
					onMovedRight?.Invoke();
				}

				if(onKeyboardKeyPressed != null) {
					Keys[] pressedKeys = currentKeyboard.GetPressedKeys();
					for(int i = 0; i < pressedKeys.Length; ++i) {
						onKeyboardKeyPressed.Invoke(pressedKeys[i]);
					}
				}

				lastKeyboardState = currentKeyboard;
			}
		}

		bool IsKeysPressed(KeyboardState last, KeyboardState cur, Keys[] keys) {
			for(int i = 0; i < keys.Length; ++i) {
				if(last.IsKeyUp(keys[i]) && cur.IsKeyDown(keys[i]))
					return true;
			}
			return false;
		}

		bool IsKeysReleased(KeyboardState last, KeyboardState cur, Keys[] keys) {
			for(int i = 0; i < keys.Length; ++i) {
				if(last.IsKeyDown(keys[i]) && cur.IsKeyUp(keys[i]))
					return true;
			}
			return false;
		}

#endregion
	}

}