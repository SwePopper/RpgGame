using System;

using Microsoft.Xna.Framework;

namespace RpgGame {

	/// <summary>
	/// Utility class with static functions and extentions
	/// </summary>
	public static class Utils {
		
		/// Check if point's x and y is zero
		public static bool IsZero(this Point point) {
			return point.X == 0 && point.Y == 0;
		}

		/// Remove item at index from array
		public static void RemoveAt<T>(this T[] array, int index) {
			if(array == null || array.Length < index) {
				return;
			} else if(array.Length - 1 != index) {
				System.Array.Copy(array, index + 1, array, index, array.Length - index - 1);
			}
			System.Array.Resize(ref array, array.Length - 1);
		}

		/// Remove all items matching predicate
		public static void RemoveAll<T>(this T[] array, System.Predicate<T> match) {
			for(int i = 0; i < array.Length; ++i) {
				if(match(array[i])) {
					array.RemoveAt(i--);
				}
			}
		}

		/// Checks array if it is null or empty
		public static bool IsNullOrEmpty<T>(this T[] array) {
			return array == null || array.Length <= 0;
		}
		
		/// Next random number in a gaussian distribution
		public static double NextGaussian(this Random random, double mean = 0, double sigma = 1) {
			double u1 = random.NextDouble();
			double u2 = random.NextDouble();
			
			return mean + sigma * Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
		}
		
		/// Next random number in a gaussian distribution between 0.0 and 1.0
		public static double NextGaussian01(this Random random) {
			return GameMath.Clamp(random.NextGaussian(0.5, 0.25), 0.0, 1.0);
		}
		
		/// Swizzle xy components
		public static Vector2 xy(this Vector3 value) {
			return new Vector2(value.X, value.Y);
		}
		
		/// Swizzle xz components
		public static Vector2 xz(this Vector3 value) {
			return new Vector2(value.X, value.Z);
		}
		
		/// Swizzle yz components
		public static Vector2 yz(this Vector3 value) {
			return new Vector2(value.Y, value.Z);
		}
		
		/// Convert Vector2 to Vector3 with z == 0f
		public static Vector3 ToVector3(this Vector2 value) {
			return new Vector3(value.X, value.Y, 0f);
		}
	}

}