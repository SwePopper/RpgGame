using System.Diagnostics;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {

	/// <summary>
	/// Debug class for handling logging output to both System.Console and on the screen.
	/// </summary>
	public static class Debug {
		static ContentManager contentManager = null;
		static SpriteBatch debugSpriteBatch = null;
		static SpriteFont debugFont = null;

		class DebugMessage {
			public double time;
			public Color color;
			public string message;
		}
		static DebugMessage[] debugMessages = new DebugMessage[5];

		/// <summary>
		/// Initializes the font and spritebatch. Call this from LoadContent.
		/// </summary>
		[Conditional("DEBUG")]
		public static void Initialize(GraphicsDevice graphicsDevice) {
			contentManager = new ContentManager(App.instance.Services, App.instance.contentRoot);
			debugSpriteBatch = new SpriteBatch(graphicsDevice);
			debugFont = contentManager.Load<SpriteFont>("Fonts/Hud");
		}

		/// <summary>
		/// Draws the 5 most recent debug log.
		/// </summary>
		[Conditional("DEBUG")]
		public static void Draw() {
			Rectangle titleSafeArea = debugSpriteBatch.GraphicsDevice.Viewport.TitleSafeArea;

			Vector2 hudLocation = new Vector2(titleSafeArea.X, titleSafeArea.Y);

			debugSpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);

			Color color = Time.isFrameRunningSlowly ? Color.Red : Color.White;

			string timingString = "DT: " + Time.deltaTime.ToString();
			DrawShadowedString(debugFont, timingString, hudLocation, color, debugSpriteBatch);

			float stringHeight = debugFont.MeasureString(timingString).Y  * 1.2f;
			timingString = "SmoothDT: " + Time.smoothDeltaTime.ToString();
			DrawShadowedString(debugFont, timingString, hudLocation + new Vector2(0.0f, stringHeight), color, debugSpriteBatch);

			timingString = "FPS: " + (1.0 / Time.smoothDeltaTime).ToString();
			DrawShadowedString(debugFont, timingString, hudLocation + new Vector2(0.0f, stringHeight*2), color, debugSpriteBatch);

			hudLocation = new Vector2(titleSafeArea.X, titleSafeArea.Y + titleSafeArea.Height - stringHeight);
			for(int i = 0; i < debugMessages.Length; ++i) {
				if(debugMessages[i] == null || string.IsNullOrEmpty(debugMessages[i].message))
					continue;

				debugMessages[i].time += Time.deltaTime;
				if(debugMessages[i].time > 4.0) {
					debugMessages[i].message = "";
					continue;
				}

				DrawShadowedString(debugFont, debugMessages[i].message, hudLocation, debugMessages[i].color, debugSpriteBatch);
				hudLocation.Y -= stringHeight;
			}

			debugSpriteBatch.End();
		}

		/// <summary>
		/// Log info with white text.
		/// </summary>
		/// <param name="message">Log message.</param>
		[Conditional("DEBUG")]
		public static void LogInfo(string message) {
			Log("Info: " + message, Color.White);
			System.Console.WriteLine(message);
		}

		/// <summary>
		/// Log warning with yellow text.
		/// </summary>
		/// <param name="message">Log message.</param>
		[Conditional("DEBUG")]
		public static void LogWarning(string message) {
			Log("Warning: " + message, Color.Yellow);
			System.Console.WriteLine("Warning: " + message);
		}

		/// <summary>
		/// Log error with red text.
		/// </summary>
		/// <param name="message">Log message.</param>
		[Conditional("DEBUG")]
		public static void LogError(string message) {
			Log("ERROR: " + message, Color.Red);
			System.Console.Error.WriteLine(message);
		}

		/// <summary>
		/// Log message with custom text color.
		/// </summary>
		/// <param name="message">Log message.</param>
		/// <param name="color">Message color.</param>
		[Conditional("DEBUG")]
		public static void Log(string message, Color color) {
			DebugMessage msg = new DebugMessage();
			msg.time = 0.0;
			msg.message = message;
			msg.color = color;
			for(int i = debugMessages.Length - 1; i >= 1; --i) {
				debugMessages[i] = debugMessages[i - 1];
			}
			debugMessages[0] = msg;
		}

		private static void DrawShadowedString(SpriteFont font, string value, Vector2 position, Color color, SpriteBatch spriteBatch) {
			spriteBatch.DrawString(font, value, position + new Vector2(1.0f, 1.0f), Color.Black);
			spriteBatch.DrawString(font, value, position, color);
		}
	}

}