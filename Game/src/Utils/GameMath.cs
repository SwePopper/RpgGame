using System;

using Microsoft.Xna.Framework;

namespace RpgGame {

	/// <summary>
	/// Math utility functions
	/// </summary>
	public static class GameMath {
		/// PI in double type
		public static readonly double PI = Math.PI;
		/// PI/2 in double type
		public static readonly double PIhalf = PI/2.0;
		/// PI*2 in double type
		public static readonly double PI2 = PI*2.0;
		/// PI in float type
		public static readonly float fPI = (float)Math.PI;
		/// PI/2 in float type
		public static readonly float fPIhalf = fPI/2f;
		/// PI*2 in float type
		public static readonly float fPI2 = fPI*2f;

		/// Clamp value between min and max
		public static T Clamp<T>(T value, T min, T max) where T : IComparable {
			return value.CompareTo(min) < 0 ? min :
				value.CompareTo(max) > 0 ? max :
				value;
		}

		/// Get the min value
		public static T Min<T>(T a, T b) where T : IComparable {
			return a.CompareTo(b) > 0 ? b :
				a;
		}

		/// Get the max value
		public static T Max<T>(T a, T b) where T : IComparable {
			return a.CompareTo(b) < 0 ? b :
				a;
		}

		/// Linear interpolate from two values in the amount between 0 and 1
		public static byte Lerp(byte from, byte to, double amount) {
			return (byte)(from + ((double)(to - from) * amount));
		}
		
		/// Linear interpolate from two values in the amount between 0 and 1
		public static Vector2 Lerp(Vector2 from, Vector2 to, double amount) {
			return from + ((to - from) * (float)amount);
		}
		
		/// Linear interpolate from two values in the amount between 0 and 1
		public static Vector3 Lerp(Vector3 from, Vector3 to, double amount) {
			return from + ((to - from) * (float)amount);
		}

		/// Floor value
		public static int FloorToInt(float value) {
			return (int)Math.Floor(value);
		}

		/// Floor value
		public static int FloorToInt(double value) {
			return (int)Math.Floor(value);
		}

		/// Get the magnitude of the point
		public static int Magnitude(this Point rect) {
			return FloorToInt(Math.Sqrt((double)(rect.X * rect.X + rect.Y * rect.Y)));
		}

		/// Get the magnitude squared of the point
		public static int MagnitudeSquared(this Point rect) {
			return FloorToInt(rect.X * rect.X + rect.Y * rect.Y);
		}
	}

}