using Microsoft.Xna.Framework;

namespace RpgGame {

	/// <summary>
	/// Time class similar to Unity's Time class.
	/// </summary>
	public static class Time {
		/// The time in seconds it took to complete the last frame (Read Only).
		public static double deltaTime { get { return previousDeltaTimes[0]; } }

		/// The interval in seconds at which physics and other fixed frame rate updates (like MonoBehaviour's FixedUpdate) are performed.
		public static double fixedDeltaTime { get; private set; } = 0.016;

		/// The total number of frames that have passed (Read Only).
		public static int frameCount { get; private set; } = 0;

		/// A smoothed out Time.deltaTime (Read Only).
		public static double smoothDeltaTime { get; private set; } = 0.0;

		/// The time at the beginning of this frame (Read Only). This is the time in seconds since the start of the game.
		public static double time { get; private set; } = 0.0;

		/// The scale at which the time is passing. This can be used for slow motion effects.
		public static double timeScale { get; private set; } = 1.0;

		/// The time this frame has started (Read Only). This is the time in seconds since the last level has been loaded.
		public static double timeSinceLevelLoad { get; private set; } = 0.0;

		/// The timeScale-independent time in seconds it took to complete the last frame (Read Only).
		public static double unscaledDeltaTime { get { return previousUnscaledDeltaTime[0]; } }

		/// The timeScale-independant time at the beginning of this frame (Read Only). This is the time in seconds since the start of the game.
		public static double unscaledTime { get; private set; } = 0.0;

		/// The time this frame has started (Read Only). This is the time in seconds since the last level has been loaded.
		public static double unscaledtimeSinceLevelLoad { get; private set; } = 0.0;

		/// A smoothed out Time.unscaledDeltaTime (Read Only).
		public static double unscaledSmoothDeltaTime { get; private set; } = 0.0;

		/// Same as GameTime.IsRunningSlowly
		public static bool isFrameRunningSlowly { get; private set; }
		
		/// Current frames GameTime
		public static GameTime getFrameGameTime { get; private set; }

		// Used for the smoothed delta times
		static double[] previousDeltaTimes = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		static double[] previousUnscaledDeltaTime = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

		/// <summary>
		/// Updates all the timing vars
		/// </summary>
		public static void Update(GameTime gameTime) {
			++frameCount;

			isFrameRunningSlowly = gameTime.IsRunningSlowly;

			double deltaTimeSum = 0.0;
			double unscaledDeltaTimeSum = 0.0;
			for(int i = 0; i < previousDeltaTimes.Length - 1; ++i) {
				previousDeltaTimes[i + 1] = previousDeltaTimes[i];
				previousUnscaledDeltaTime[i + 1] = previousUnscaledDeltaTime[i];

				deltaTimeSum += previousDeltaTimes[i];
				unscaledDeltaTimeSum += previousUnscaledDeltaTime[i];
			}

			previousUnscaledDeltaTime[0] = gameTime.ElapsedGameTime.TotalSeconds;
			unscaledTime = gameTime.TotalGameTime.TotalSeconds;
			unscaledtimeSinceLevelLoad += unscaledDeltaTime;

			previousDeltaTimes[0] = unscaledDeltaTime * timeScale;
			time += previousDeltaTimes[0];
			timeSinceLevelLoad += deltaTime;

			deltaTimeSum += previousDeltaTimes[0];
			unscaledDeltaTimeSum += previousUnscaledDeltaTime[0];

			smoothDeltaTime = deltaTimeSum / previousDeltaTimes.Length;
			unscaledSmoothDeltaTime = unscaledDeltaTimeSum / previousDeltaTimes.Length;
			
			getFrameGameTime = gameTime;
		}
	}

}
