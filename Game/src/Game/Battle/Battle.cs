using System;

namespace RpgGame {

	/// <summary>
	/// RPG battle class. Think Pokemon or Ni No Kuni.
	/// </summary>
	public class Battle {
		/// <summary>
		/// RPG battle class. Think Pokemon or Ni No Kuni.
		/// </summary>
		public class Member {
			/// Member name
			public string name = "";
		}

		Random random = null;

		Member[] teamOne = null;
		Member[] teamTwo = null;

		/// constructor
		/// <param name="teamOne">First team in the battle.</param>
		/// <param name="teamTwo">Second team in the battle.</param>
		/// <param name="randomSeed">Seed for the random generator.</param>
		public Battle(Member[] teamOne, Member[] teamTwo, int randomSeed = 0) {
			random = new Random(randomSeed);
			this.teamOne = teamOne;
			this.teamTwo = teamTwo;

			Debug.LogInfo("Created Battle with seed: " + randomSeed.ToString());
		}

		/// Updates the battle
		public void Update() {
		}

		/// Draws the battle
		public void Draw() {
		}
	}

}