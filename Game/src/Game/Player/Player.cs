using System;

namespace RpgGame {

	/// <summary>
	/// Main player class. It holds the player information.
	/// </summary>
	public class Player {
		/// Player name
		public string name = "";

		/// Player level
		public int level = 1;
		/// Experience in the current level
		public int experience = 0;

		/// <summary>
		/// Level to experience function.
		/// </summary>
		/// <param name="level">Level to convert to experience.</param>
		public static int LevelToExperience(int level) {
			return level*level*2;
		}

		/// <summary>
		/// Experience to level function.
		/// </summary>
		/// <param name="experience">Experience to convert to the corresponding level (floored).</param>
		public static int ExperienceToLevel(int experience) {
			return GameMath.FloorToInt(Math.Sqrt(experience / 2.0)) + 1;
		}
	}

}