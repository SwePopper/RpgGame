using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RpgGame {
	
	/// <summary>
	/// Main menu root object
	/// </summary>
	public class MainMenu : UiRoot {
		/// constructor
		public MainMenu(Ui ui) : base(ui) {}

		///
		public override void Initialize() {
			UiImage image = new UiImage(ui);
			image.texture = new Texture2D(ui.graphicsDevice, 1, 1);
			image.texture.SetData(new Color[] { Color.Black });
			image.SetHightWithAspect(ui.graphicsDevice.Viewport.Width, image.texture);
			image.drawDepth = 0;

			UiButton button = new UiButton(ui, this);
			button.bounds.Width = ui.graphicsDevice.Viewport.Width / 2;
			button.bounds.Height = 80;
			button.bounds.X = ui.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
			button.bounds.Y = ui.graphicsDevice.Viewport.Height / 2 - button.bounds.Height / 2;
			button.normalTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.normalTexture.SetData(new Color[] { Color.Green });
			button.highlightTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.highlightTexture.SetData(new Color[] { Color.Yellow });
			button.pressedTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.pressedTexture.SetData(new Color[] { Color.Pink });
			button.disabledTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.disabledTexture.SetData(new Color[] { Color.Gray });
			button.onClickEvent += OnTest;

			UiText text = new UiText(ui, button);
			text.font = contentManager.Load<SpriteFont>("Fonts/Hud");
			text.text = "Start";
			text.bounds.Size = text.font.MeasureString(text.text).ToPoint();
			text.bounds.X = button.bounds.Width / 2 - text.bounds.Width / 2;
			text.bounds.Y = button.bounds.Height / 2 - text.bounds.Height / 2;


			button = new UiButton(ui, this);
			button.bounds.Width = ui.graphicsDevice.Viewport.Width / 2;
			button.bounds.Height = 80;
			button.bounds.X = ui.graphicsDevice.Viewport.Width / 2 - button.bounds.Width / 2;
			button.bounds.Y = ui.graphicsDevice.Viewport.Height / 2 + button.bounds.Height / 2 + 20;
			button.normalTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.normalTexture.SetData(new Color[] { Color.Green });
			button.highlightTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.highlightTexture.SetData(new Color[] { Color.Yellow });
			button.pressedTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.pressedTexture.SetData(new Color[] { Color.Pink });
			button.disabledTexture = new Texture2D(ui.graphicsDevice, 1, 1);
			button.disabledTexture.SetData(new Color[] { Color.Gray });
			button.onClickEvent += OnQuit;

			text = new UiText(ui, button);
			text.font = contentManager.Load<SpriteFont>("Fonts/Hud");
			text.text = "Quit";
			text.bounds.Size = text.font.MeasureString(text.text).ToPoint();
			text.bounds.X = button.bounds.Width / 2 - text.bounds.Width / 2;
			text.bounds.Y = button.bounds.Height / 2 - text.bounds.Height / 2;

			base.Initialize();
		}

		void OnTest() {
			Debug.LogInfo("MainMenu -> TitleScreen");
		}

		void OnQuit() {
			App.instance.Exit();
		}
	}

}