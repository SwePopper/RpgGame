using Microsoft.Xna.Framework.Content;

namespace RpgGame {

	/// <summary>
	/// Main game class.
	/// </summary>
	public class RpgGame {
		ContentManager contentManager = null;
		Player player = null;

		/// constructor
		public RpgGame() {
			player = new Player();

			Debug.LogInfo("Player: " + player.name + "  level: " + player.level.ToString() + "  Exp: " + player.experience.ToString());
		}

		/// Called when the app is closing
		public void OnExiting() {
		}

		/// Called when App:LoadContent event is called
		public void LoadContent() {
			contentManager = new ContentManager(App.instance.Services, App.instance.contentRoot);

			App.instance.ui.SetCurrentRoot<MainMenu>();
		}

		/// Updates the game
		public void Update() {
		}

		/// Draws the game
		public void Draw() {
		}
	}

}