using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace RpgGame {

	/// <summary>
	/// Configuration file for the main app.
	/// </summary>
	public class Config {
		/// Main App settings
		public AppConfig appConfig = new AppConfig();
		/// Graphics settings
		public GraphicsConfig graphicsConfig = new GraphicsConfig();

		/// Load a Config from file
		public static Config Load(string path) {
			if(!File.Exists(path)) {
				Directory.CreateDirectory(Path.GetDirectoryName(path));
				return new Config();
			}

			XmlSerializer serializer = new XmlSerializer(typeof(Config));
			using(FileStream stream = new FileStream(path, FileMode.Open)) {
				return serializer.Deserialize(stream) as Config;
			}
		}

		/// Save Config to file
		public static void Save(Config config, string path) {
			XmlSerializer serializer = new XmlSerializer(typeof(Config));
			using(FileStream stream = new FileStream(path, FileMode.Create)) {
				serializer.Serialize(stream, config);
			}
		}
	}

	/// <summary>
	/// Main app settings
	/// </summary>
	public class AppConfig {
		/// Name of the application
		[XmlAttribute("name")]
		public string name = "";
		/// Organization that made tha application
		[XmlAttribute("organization")]
		public string organization = "";
	}

	/// <summary>
	/// Graphics settings
	/// </summary>
	public class GraphicsConfig {
		/// Screen Width
		[XmlAttribute("width")]
		public int width = 1024;
		/// Screen Height
		[XmlAttribute("height")]
		public int height = 768;
		/// Screen index
		[XmlAttribute("screen_index")]
		public int screenIndex = 0;
		/// Fullscreen
		[XmlAttribute("fullscreen")]
		public bool fullscreen = false;
	}
}