using System;
using System.Reflection;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Windows.Forms;

namespace RpgGame {

	/// <summary>
	/// Main App class
	/// </summary>
	public class App : Game {
		/// Global App instance.
		public static App instance { get; private set; } = null;

		/// The root content directory.
		public string contentRoot { get; private set; } = "";
		/// Ui Manager
		public Ui ui { get; private set; } = null;

		/// Configuration
		public Config config = null;
		private string configPath = "";

		private GraphicsDeviceManager graphics = null;
		private SpriteBatch spriteBatch = null;
		private RpgGame game = null;

		/// constructor
		public App() {
			instance = this;

			// Setup graphics
			graphics = new GraphicsDeviceManager(this);
			graphics.IsFullScreen = false;
			graphics.SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight;

			IsMouseVisible = true;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize() {
			// Load config file
			configPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/" + Assembly.GetExecutingAssembly().GetName().Name + "/config.xml";
			config = Config.Load(configPath);

			graphics.PreferredBackBufferWidth = config.graphicsConfig.width;
			graphics.PreferredBackBufferHeight = config.graphicsConfig.height;
			graphics.ApplyChanges();

			config.graphicsConfig.screenIndex = GameMath.Clamp(config.graphicsConfig.screenIndex, 0, Screen.AllScreens.Length-1);
			System.Drawing.Rectangle screen = Screen.AllScreens[config.graphicsConfig.screenIndex].Bounds;

			Window.Position = new Point(screen.X + (screen.Width / 2) - (config.graphicsConfig.width / 2),
										screen.Y + (screen.Height / 2) - (config.graphicsConfig.height / 2));

			graphics.IsFullScreen = config.graphicsConfig.fullscreen;

			// Setup Content
			if(System.IO.Directory.Exists("Content")) {
				contentRoot = Content.RootDirectory = "Content";
			} else {
				contentRoot = Content.RootDirectory = System.IO.Path.GetFullPath("bin/Content");
				if(!System.IO.Directory.Exists(contentRoot)) {
					throw new System.Exception("Failed to find Content Directory!");
				}
			}

			ui = new Ui();
			ui.Initialize();

			game = new RpgGame();

			base.Initialize();
		}

		/// Exiting the application
		protected override void OnExiting(object sender, EventArgs args) {
			base.OnExiting(sender, args);

			Debug.LogInfo("OnExiting");

			game?.OnExiting();

			config.graphicsConfig.width = Window.ClientBounds.Width;
			config.graphicsConfig.height = Window.ClientBounds.Height;

			System.Drawing.Point pos = new System.Drawing.Point(Window.Position.X, Window.Position.Y);
			for(int i = 0; i < Screen.AllScreens.Length; ++i) {
				if(Screen.AllScreens[i].Bounds.Contains(pos)) {
					config.graphicsConfig.screenIndex = i;
					break;
				}
			}

			config.graphicsConfig.fullscreen = graphics.IsFullScreen;

			Config.Save(config, configPath);
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent() {
			spriteBatch = new SpriteBatch(GraphicsDevice);
			
			ui?.LoadContent(GraphicsDevice);
			game?.LoadContent();

			Debug.Initialize(GraphicsDevice);
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime) {
			Time.Update(gameTime);

			game?.Update();
			ui.Update();
			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime) {
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			game?.Draw();
			ui.Draw();
			Debug.Draw();
			base.Draw(gameTime);
		}
	}

}